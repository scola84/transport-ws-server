'use strict';

const Error = require('@scola/error');
const EventHandler = require('@scola/events');

class Server extends EventHandler {
  constructor(filters, serial, message, server, connection, options) {
    super();

    this.filters = filters;
    this.messageProvider = message;
    this.serverFactory = server;
    this.connectionProvider = connection;

    this.serial = serial
      .setProcessor(this.processVerification.bind(this));

    this.options = options.assign({
      headers: {},
      verifyClient: this.handleVerification.bind(this)
    });

    this.server = null;
  }

  getOptions() {
    return this.options;
  }

  bind(options) {
    this.emit('debug', this, 'bind', options);

    this.options.assign(options);

    this.server = this.serverFactory
      .create(this.options.toJSON());
    this.addListeners();

    return this;
  }

  release() {
    this.emit('debug', this, 'release');

    this.server.close();
    this.removeListeners();

    return this;
  }

  createMessage() {
    this.emit('debug', this, 'createMessage');
    return this.messageProvider.get();
  }

  addListeners() {
    this.bindListener('connection', this.server, this.handleConnection);
    this.bindListener('error', this.server, this.handleError);
    this.bindListener('headers', this.server, this.handleHeaders);
  }

  removeListeners() {
    this.unbindListener('connection', this.server, this.handleConnection);
    this.unbindListener('error', this.server, this.handleError);
    this.unbindListener('headers', this.server, this.handleHeaders);
  }

  handleVerification(info, callback) {
    this.emit('debug', this, 'handleVerification', info);

    this.serial
      .process(this.filters, [info])
      .then(() => callback(true))
      .catch(() => callback(false));
  }

  processVerification(filter, [info]) {
    this.emit('debug', this, 'processVerification', info);
    return filter.get().connect(info.req);
  }

  handleConnection(connection) {
    this.connectionProvider
      .get()
      .once('open', this.handleOpen.bind(this))
      .open(connection, this);
  }

  handleOpen(connection) {
    this.emit('connection', connection);
  }

  handleError(error) {
    this.emit('error', new Error('transport_server_error', {
      origin: error
    }));
  }

  handleHeaders(serverHeaders) {
    this.emit('debug', this, 'handleHeaders', serverHeaders);

    const headers = this.options.get('headers');

    Object.keys(headers).forEach((name) => {
      serverHeaders.push(name + ':' + headers[name]);
    });
  }
}

module.exports = Server;
