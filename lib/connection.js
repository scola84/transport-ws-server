'use strict';

const Abstract = require('@scola/transport-ws-common');

class Connection extends Abstract.Connection {
  open(connection) {
    this.connection = connection;
    this.bindListeners();

    this.emit('open', this);
    return this;
  }

  handleReceiveError(message, error) {
    this.connection.send(error.toString());
    super.handleReceiveError(message, error);
  }
}

module.exports = Connection;
